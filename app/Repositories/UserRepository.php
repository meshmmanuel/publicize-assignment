<?php

namespace App\Repositories;

use App\User;
use App\Http\Resources\User as UserResource;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    /**
     * Get all users
     */
    public function all()
    {
        $users = User::all();
        return UserResource::collection($users);
    }

    /**
     * Create a new User
     */
    public function create($data)
    {
        $user =  User::create([
            'name' => $data->name,
            'email' => $data->email,
            'is_admin' => $data->is_admin,
            'password' => Hash::make($data->password)
        ]);

        $token = $user->createToken('publicize')->accessToken;

        return new UserResource($user);
    }

    /**
     * Get one user
     */
    public function get($id)
    {
        $user = User::findOrFail($id);
        return new UserResource($user);
    }

    /**
     * Update existing user record
     */
    public function update(int $id, $data)
    {
        $user = User::findOrFail($id);

        if (isset($data->name)) {
            $user->name = $data->name;
        }

        if (isset($data->email)) {
            $user->email = $data->email;
        }

        if (isset($data->is_admin)) {
            $user->is_admin = $data->is_admin;
        }

        if (isset($data->password)) {
            $user->password = Hash::make($data->password);
        }

        if (isset($data->access_code)) {
            $user->access_code = $data->access_code;
        }

        $user->save();

        return new UserResource($user);
    }

    /**
     * Delete user record
     */
    public function delete(int $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return new UserResource($user);
    }
}
