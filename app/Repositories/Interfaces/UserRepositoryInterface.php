<?php

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
    /**
     * Get all Users
     */
    public function all();

    /**
     * Create a new User
     * @param $data
     */
    public function create($data);

    /**
     * Get one user
     * @param int $id
     */
    public function get(int $id);

    /**
     * Update existing user record
     * @param int $id
     * @param $data
     */
    public function update(int $id, $data);

    /**
     * Delete a user
     * @param int $id
     */
    public function delete(int $id);

}
