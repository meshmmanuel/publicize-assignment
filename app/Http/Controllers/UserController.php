<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->middleware('is.admin')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'success' => true,
            'message' => 'Retrieved list of users',
            'data' => $this->userRepository->all()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'is_admin' => 'required|integer',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ];

        $this->validate($request, $rules);

        $user = $this->userRepository->create($request);

        // Send Mail after 2 minutes
        SendEmail::dispatch($user)
            ->delay(now()->addMinutes(2));

        return response()->json([
            'success' => true,
            'message' => 'Created a user',
            'data' => $user
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'success' => true,
            'message' => 'Retrieved a user',
            'data' => $this->userRepository->get($id)
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'email' => 'email|unique:users, email,' . $id,
            'password' => 'min:6|confirmed'
        ];

        $this->validate($request, $rules);

        return response()->json([
            'success' => true,
            'message' => 'Updated a user record',
            'data' => $this->userRepository->update($id, $request)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json([
            'success' => true,
            'message' => 'Deleted a user',
            'data' => $this->userRepository->delete($id)
        ], 200);
    }
}
