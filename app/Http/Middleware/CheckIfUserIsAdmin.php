<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfUserIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->is_admin == 0) {
            return response()->json([
                "success" => false,
                "message" => "You don't have administrative rights",
            ], 405);
        }
        return $next($request);
    }
}
