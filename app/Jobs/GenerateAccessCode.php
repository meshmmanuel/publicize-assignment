<?php

namespace App\Jobs;

use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateAccessCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $userRepository;
    private $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepository, $user)
    {
        $this->userRepository = $userRepository;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Create Access Code
        $data = (object) [
            'access_code' => uniqid()
        ];

        // Update User Record
        $this->userRepository->update($this->user->id, $data);
    }
}
